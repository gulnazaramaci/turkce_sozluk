import 'package:flutter/material.dart';
import 'package:turkce_sozluk/widget/HomeCard.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  FocusNode _fNode;
  bool inputFocus = false;

  @override
  void initState() {
    super.initState();
    _fNode = FocusNode();

    _fNode.addListener(() {
      setState(() {
        if(_fNode.hasFocus){
          inputFocus = true;
        }else{
          inputFocus = false;
        }
      });
    });
  }

  @override
  void dispose() {
    _fNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color.fromRGBO(248, 248, 248, 1),
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              AnimatedContainer(
                duration: const Duration(milliseconds: 200),
                curve: Curves.fastOutSlowIn,
                height: inputFocus == false ? 340.0 : 0.0,
                color: Color.fromRGBO(225, 30, 60, 1),
                child: Center(
                  child: Image(image: AssetImage("images/logo.png"),),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                    color: Color.fromRGBO(248, 248, 248, 1),
                    child: ListView(
                      children: <Widget>[
                        Visibility(
                          maintainSize: inputFocus == true ? false : true,
                          maintainAnimation: inputFocus == true ? false : true,
                          maintainState: inputFocus == true ? false : true,
                          visible: inputFocus == true ? false : true,
                          child: Column(
                            children: <Widget>[
                              HomeCard("Bir Kelime","on para","çok az (para)"),
                              SizedBox(
                                height: 40,
                              ),
                              HomeCard("Bir Deyim, Atasözü","siyem siyem ağlamak","hafif hafif, ince ince durmadan gözyaşı dökmek."),
                            ],
                          ),
                        ),
                      ],
                    ),

                  ),
                ),
              ),
            ],
          ),
          AnimatedPositioned(
            duration: const Duration(milliseconds: 200),
            curve: Curves.fastOutSlowIn,
            top:  inputFocus == false ? 310.0 : 60.0,
            left: 15,
            right: 15,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 60,
                  width:  inputFocus == false ? screenWidth - 30: screenWidth - 110 ,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 6.0,
                      ),
                    ],
                  ),
                  child: Center(
                    child: TextField(
                      autofocus: false,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search),
                        //contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                      ),
                      focusNode: _fNode,
                    ),
                  ),
                ),
                Visibility(
                  maintainSize: inputFocus == false ? false : true,
                  maintainAnimation: inputFocus == false ? false : true,
                  maintainState: inputFocus == false ? false : true,
                  visible: inputFocus == false ? false : true,
                  child: Container(
                   width: 80,
                   child: Center(
                     child: InkWell(
                       child: Text("Vazgeç",style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 1.4),),
                       onTap: (){FocusScope.of(context).requestFocus(FocusNode());},
                     ),
                   ),
                 ),
               ),
              ],
            ),
          ),

        ],
      ),
    );
  }
}
