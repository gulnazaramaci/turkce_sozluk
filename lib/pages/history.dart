import 'package:flutter/material.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Geçmiş'),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(225, 30, 60, 1),
      ),
    );
  }
}
