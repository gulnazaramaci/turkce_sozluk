import 'package:flutter/material.dart';
import 'package:turkce_sozluk/pages/dashboard.dart';
import 'package:turkce_sozluk/pages/favorite.dart';
import 'package:turkce_sozluk/pages/history.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int currentTab = 0;
  final List<Widget> screens = [
    Dashboard(),
    Favorite(),
    History(),
  ];

  Widget currentScreen = Dashboard();

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: Container(
        width: 80.0,
        height: 80.0,
        child: FloatingActionButton(
          child: Icon(Icons.search),
          backgroundColor: Color.fromRGBO(225, 30, 60, 1),
          onPressed: (){
            setState(() {
              currentScreen = Dashboard();
              currentTab = 0;
            },);
          },
        ),
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 60,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                MaterialButton(
                  minWidth: 40,
                  onPressed: (){
                    setState(() {
                      currentScreen = History();
                      currentTab = 1;
                    },);
                  },
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.history,color: currentTab == 1 ? Color.fromRGBO(225, 30, 60, 1) : Colors.grey,size: 30,),
                      Icon(Icons.fiber_manual_record,color: currentTab == 1 ? Color.fromRGBO(225, 30, 60, 1) : Colors.grey,size: 5,),
                    ],
                  ),
                ),
                MaterialButton(
                  minWidth: 40,
                  onPressed: (){
                    setState(() {
                      currentScreen = Favorite();
                      currentTab = 2;
                    },);
                  },
                  child: Column(
                    children: <Widget>[
                      Icon(Icons.bookmark,color: currentTab == 2 ? Color.fromRGBO(225, 30, 60, 1) : Colors.grey,size: 30,),
                      Icon(Icons.fiber_manual_record,color: currentTab == 2 ? Color.fromRGBO(225, 30, 60, 1) : Colors.grey,size: 5,),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
