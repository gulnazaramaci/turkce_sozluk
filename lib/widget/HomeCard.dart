import 'package:flutter/material.dart';

Widget HomeCard(title,baslik,aciklama){
  return  Container(
    child: Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(title,style: TextStyle(color: Color.fromRGBO(117, 130, 145, 1),),),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          padding: const EdgeInsets.all(15.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            color: Colors.white,
          ),
          child: Container(
            padding: EdgeInsets.fromLTRB(10.0, 0, 0, 0),
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(
                  color:  Color.fromRGBO(227, 229, 232, 1),
                  width: 3.0,
                ),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(baslik,style: TextStyle(fontSize: 21,color:Color.fromRGBO(10, 21, 31, 1),fontWeight: FontWeight.bold ),),
                SizedBox(
                  height: 8,
                ),
                Text(aciklama,style: TextStyle(fontSize: 14,color:Color.fromRGBO(72, 81, 91, 1),fontWeight: FontWeight.w300),),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}